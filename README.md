This is the source code and other files for the article "Wireless Weather Station Uses Arduino", that I wrote for Circuit Cellar Magazine (September 2018 #338).

One of the main points of the article is to show how you can implement a very simple communication protocol to transmit data wirelessly. It's very simple, it works, but it's not robust. You may want to try some wireless communication libraries already available for the Arduino plaform, that support the same ASK modules I used in this project.

Ve la version en español de este artículo en: https://tecbolivia.com/index.php/articulos-y-tutoriales-microcontroladores/97-estacion-meteorologica-inalambrica-usando-modulos-ask-de-bajo-costo-y-arduino

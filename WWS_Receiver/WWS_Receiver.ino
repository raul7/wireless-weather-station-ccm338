/* 
 * *****************************************************************************
 * Wireless Weather Station Using Low Cost ASK Modules and Arduino
 *
 *
 * Transmitting Node Hardware: 
 *                       Arduino Pro Mini ATmega328P, Generic 433/315MHz ASK RF 
 *                       Transmitter, DHT22 Relative Humidity and Temperature 
 *                       Sensor, Analog Anemometer and Analog Wind Vane.
 * Receiving Node Hardware:    
 *                       Arduino Pro Mini ATmega328P, Generic 433/315MHz ASK RF 
 *                       Receiver, 0.96" OLED SPI Display.
 * Version:              0.1 (February, 5, 2018)
 * Author:               Raul Alvarez Torrico
********************************************************************************
*/

/*  The max baud rate for these ASK modules is 2400 (perhaps up to 4800 under
 *  specific conditions), nevertheless, with higher baud rates the range for a 
 *  reliable communication without errors decreases. To achieve longer distances
 *  it is better to use baud rates equal or less than 1200.
 */

/* Include libraries */
#include <SoftwareSerial.h> // Software serial (bit-banging) library
#include <Arduino.h>        // Required for U8g2 library
#include <U8g2lib.h>        // U8g2 OLED display library

// The following copied and pasted from a U8g2 library example. Technically, we need
// just one of them (SPI), but just in case we opt for an I2C OLED display instead
#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

/* Define constants */
#define DEBUG_BAUD_RATE 250000  // Serial monitor debugging baud rate

// Constants for wireless communication
#define WIRELESS_BAUD_RATE 1200 // Wireless communication baud rate

// Some constants for the communication protocol
#define SYNC_BYTE   0xAA        // For synchronizing baud rate at the receiving node
#define START_BYTE  0x7E        // Frame start delimiter
#define DEST_ADDRESS_BYTE 0x11  // This node's address (receiving node)
#define HEADER_SIZE  5          // Frame header size (START, SRC_ADDR, DST_ADDR, 
                                // ID, LENGHT)

// I/O pins
#define STATUS_LED 13         // LED to show system status

// OLED LCD: Define vertical coordinates in pixels for 4 text rows, 
// based on the height of the font being used (10 pixels height font).
#define LCD_FONT_HEIGHT 10
#define LCD_ROW_SPACING 6
#define LCD_ROW_0 (LCD_FONT_HEIGHT + 5) // (Add margin of 5 for 1st. row)
#define LCD_ROW_1 (LCD_ROW_0 + LCD_FONT_HEIGHT + LCD_ROW_SPACING)
#define LCD_ROW_2 (LCD_ROW_1 + LCD_FONT_HEIGHT + LCD_ROW_SPACING)
#define LCD_ROW_3 (LCD_ROW_2 + LCD_FONT_HEIGHT + LCD_ROW_SPACING)
#define LCD_ROW_4 (LCD_ROW_3 + LCD_FONT_HEIGHT + LCD_ROW_SPACING)

/* Declare global variables */
bool status_led_state = false; // Stores the activity LED's current state

// This is to count the received frames, just for debugging. NOTE: When the counter
// reaches 65535, it overflows and starts to count again from zero:
unsigned int num_received_frames = 0;

byte received_source_address; // To store the received source address
byte received_dest_address;   // To store the received destination address
byte received_id_byte;        // To store the ID of the received frame
  
int i_temperature;            // For storing received temperature as an integer
int i_humidity;               // For storing received humidity as an integer
unsigned int i_wind_speed;    // For storing received wind velocity as an integer
float f_wind_speed;           // For calculating wind speed as a float
byte b_wind_dir;              // For storing received encoded cardinal direction
String s_wind_dir;            // For decoding cardinal direction into 1-2 
                              // character achronym

// Enumeration for encoding eight cardinal wind directions
enum {
  NOT_RECOGNIZED = 0,
  NORTH,
  NORTH_EAST,
  EAST,
  SOUTH_EAST,
  SOUTH,
  SOUTH_WEST,
  WEST,
  NORTH_WEST
};

/* Create objects */
// SPI OLED object. Copied and pasted from a u8g2 example:
U8G2_SSD1306_128X64_NONAME_1_4W_SW_SPI u8g2(U8G2_R0, 
                                            /* clock=*/ 12, 
                                            /* data=*/ 11, 
                                            /* cs=*/ 10, 
                                            /* dc=*/ 9, 
                                            /* reset=*/ 8);
SoftwareSerial wlessSerial(2, 3); // SoftwareSerial object for serial wireless 
                                  // communication, pins are: (RX, TX)
                                  
/* Arduino setup function */
void setup (void)
{
  /* Initialize serial communications */
  Serial.begin(DEBUG_BAUD_RATE);          // Serial debugging
  wlessSerial.begin(WIRELESS_BAUD_RATE);  // Serial wireless comunication
  
  /* Initialize I/O ports */
  pinMode(STATUS_LED, OUTPUT);

  /* Initialize OLED display */
  u8g2.begin();  

  // Print welcome message in the OLED display
  u8g2.firstPage();
    do {
      u8g2.setFont(u8g2_font_ncenB10_tr);
      u8g2.drawStr(0,LCD_ROW_0,"Wless Weather");
      u8g2.drawStr(0,LCD_ROW_1,"Station");
    } while ( u8g2.nextPage() );
    
  /* Blink LED to visualize externally that setup is complete */
  Blink(STATUS_LED, 1, 400); // (pin, num_bliks, period)
}

/* Arduino main loop */
void loop (void)
{   
  /* Receive, decompose and extract payload from frames */
  Receive();
}

/*******************************************************************************
*                               RECEIVE
* Description   :    Receives and decomposes a frame
********************************************************************************/
void Receive (void)
{
  // Start to process a frame only if the serial buffer has a number of bytes 
  // equal or greater than the frame header's size
  if (wlessSerial.available() >= HEADER_SIZE)    
  {
    // Uncomment this line to see how noise is permanently catched by the receiver!
//    Serial.println("HEADER_SIZE OK");  

    // Try to read a start byte, if found, process the next bytes (there's a frame ahead)
    if (wlessSerial.read() == START_BYTE)
    { 
      // Print to IDE's serial monitor just for debugging
      Serial.println("--- Frame reception ----");
      Serial.println("START_BYTE OK");

      received_source_address = wlessSerial.read(); // Read the received source address byte
      received_dest_address = wlessSerial.read();   // Read the received destination address byte
      
      // Verify that the received frame is addressed to this node
      if (received_dest_address == DEST_ADDRESS_BYTE)  
      {   
        Serial.println("DEST_ADDRESS_BYTE OK"); // Print to IDE's serial monitor just for debugging

        received_id_byte = wlessSerial.read();  // Read the ID of the received frame
        byte data_len = wlessSerial.read();     // Read the data length

        // Wait until the complete payload, plus the checksum byte have been received
        while(wlessSerial.available() <= data_len)
          ; // Do nothing, wait for at least 'data_len' bytes to arrive

        // Create an array in memory to store the received temperature bytes
        byte temp_array[2] = { 0 };
        // Read and store received temperature bytes
        temp_array[0] = wlessSerial.read();
        temp_array[1] = wlessSerial.read();

        // Create an array in memory to store the received r. humidity bytes
        byte hum_array[2] = { 0 };
        // Read and store received humidity bytes
        hum_array[0] = wlessSerial.read();
        hum_array[1] = wlessSerial.read();

        // Create an array in memory to store the received wind speed bytes
        byte wspeed_array[2] = { 0 };
        // Read and store received wind speed bytes
        wspeed_array[0] = wlessSerial.read();
        wspeed_array[1] = wlessSerial.read();

        // Receive encoded wind direction byte
        b_wind_dir = wlessSerial.read();

        // Calculate local checksum
        byte local_checksum = temp_array[0] + temp_array[1] + hum_array[0] + hum_array[1] 
                            + wspeed_array[0] + wspeed_array[1] + b_wind_dir;
        
        // Print to IDE's serial monitor just for debugging
        Serial.print("local_checksum: ");
        Serial.println(local_checksum);

        // Read the checksum byte (last byte in the frame)
        byte remote_checksum = wlessSerial.read();

        // Print to IDE's serial monitor just for debugging
        Serial.print("remote_checksum: ");
        Serial.println(remote_checksum);

        // Verify checksum
        if (remote_checksum + local_checksum == 0xFF)
        { // At this point we know that a frame has been received without errors!
          // So, we proceed to replace persistent weather values that should be 
          // displayed later in the LCD
          
          // Copy the two received temperature bytes from memory to an integer variable
          memcpy(&i_temperature, &temp_array, sizeof(i_temperature));
  
          // Divide temperature by 100 and store it as float
          // (recall that the temperature data at the source node is 
          // multiplied by 100 before is sent)
          float f_temperature = i_temperature/100.0; 

          // Copy the two received humidity bytes from memory to an integer variable
          memcpy(&i_humidity, &hum_array, sizeof(i_humidity));
          
          // Divide humidity by 100 and store it in a float
          // (recall that the humidity data at the source node is 
          // multiplied by 100 before is sent)
          float f_humidity = i_humidity/100.0;
        
          // Copy the two received wind velocity bytes from memory, as an integer
          memcpy(&i_wind_speed, &wspeed_array, sizeof(i_wind_speed));

          // Divide wind speed by 100 and store it in a float
          // (recall that the wind speed data at the source node is 
          // multiplied by 100 before is sent)
          f_wind_speed = i_wind_speed/100.0;

          // Decode received wind direction into 1-2 character cardinal direction achronym
          s_wind_dir = Get_Windvane_Dir(b_wind_dir);

          // Calculate Dew Point using received temperature and humidity values
          // See: https://en.wikipedia.org/wiki/Dew_point
          float f_dew_point = Calc_Dewpoint(f_humidity, f_temperature);
        
          ++num_received_frames; // Increment received frames counter
                                 // this is just for debugging

          // Change status LED's state to externally visualize that
          // a frame has been received succesfully. With every received
          // frame the LED goes ON/OFF alternaly
          status_led_state = !status_led_state;
          digitalWrite(STATUS_LED, status_led_state);

          // Print to IDE's serial monitor just for debugging
          Serial.println(">>> Checksum OK!");
          Serial.print("num_received_frames:");
          Serial.println(num_received_frames);

          Serial.print("num_received_frames: ");
          Serial.print(num_received_frames);
          Serial.print("\treceived_id_byte: ");
          Serial.println(received_id_byte);
          
          Serial.print("f_temperature: ");
          Serial.print(f_temperature);
          Serial.print("\tf_humidity: ");
          Serial.print(f_humidity);
          Serial.print("\tf_dew_point: ");
          Serial.println(f_dew_point);
          
          Serial.print("i_wind_speed: ");
          Serial.print(i_wind_speed);
          Serial.print("\tb_wind_dir: ");
          Serial.print(b_wind_dir);
          Serial.print("\ts_wind_dir: ");
          Serial.println(s_wind_dir);

          /* Visualize received data on the LCD */
          u8g2.firstPage();
          do {
            u8g2.clearBuffer();                 // clear the internal memory
            u8g2.setFont(u8g2_font_ncenB10_tr); // Set font
            
            // Print text header
            u8g2.drawStr(0,LCD_ROW_0,"Weather Station");
            
            // Print temperature
            u8g2.drawStr(0,LCD_ROW_1,"T:");
            u8g2.setCursor(20, LCD_ROW_1);
            u8g2.print(f_temperature);
            
            // Print wind speed
            u8g2.drawStr(60,LCD_ROW_1,"S:");
            u8g2.setCursor(80, LCD_ROW_1);
            u8g2.print(f_wind_speed);
            
            // Print r. humidity
            u8g2.drawStr(0,LCD_ROW_2,"H:");
            u8g2.setCursor(20, LCD_ROW_2);
            u8g2.print(f_humidity);
            
            // Print wind direction
            u8g2.drawStr(60,LCD_ROW_2,"WD:");
            u8g2.setCursor(95, LCD_ROW_2);
            u8g2.print(s_wind_dir);
            
            // Print calculated dew point
            u8g2.drawStr(0,LCD_ROW_3,"DP:");
            u8g2.setCursor(30, LCD_ROW_3);
            u8g2.print(f_dew_point);
            
            // Print received frame's ID... just because we have some space left
            u8g2.drawStr(70,LCD_ROW_3,"ID:");
            u8g2.setCursor(95, LCD_ROW_3);
            u8g2.print(received_id_byte);
            
          } while ( u8g2.nextPage() );
        }
      }
    }
  }
}

/*******************************************************************************
*                                 BLINK
* Description   :    Blinks the status LED
********************************************************************************/
void Blink (byte led_pin, int times, int period) {
  for (int i = 0; i < times; i++) {
    digitalWrite(led_pin, HIGH);
    delay(period >> 2);
    digitalWrite(led_pin, LOW);
    delay(period >> 2);
  }
}

/*******************************************************************************
*                                 CALC_DEWPOINT
* Description   :    Calculates dew point using temperature and humidity
********************************************************************************/
float Calc_Dewpoint (float RH, float T )
{
  // Dew point formula taken from:
  // http://irtfweb.ifa.hawaii.edu/~tcs3/tcs3/Misc/Dewpoint_Calculation_Humidity_Sensor_E.pdf
  
  float H  = (log10(RH)-2)/0.4343 + (17.62*T)/(243.12+T); 
  float Dp = 243.12*H/(17.62-H);       // this is the dew point in Celsius
  
  return Dp;
}

/*******************************************************************************
*                                 GET_WINDVANE_DIR
* Description   :    Decodes a wind vane 'byte' value into
*                    1-2 character cardinal wind direction achronym
********************************************************************************/
String Get_Windvane_Dir (byte b_dir)
{
  String s_dir = "-"; // 1-2 character wind direction achronym

  // Test the wind vane 'byte' value
  switch (b_dir) 
  {
    // Assign 1-2 character achronym according to encoded wind direction
    // received remotely
    case NORTH:
      s_dir = "N";
    break;
    
    case NORTH_EAST:
      s_dir = "NE";
    break;
    
    case EAST:
      s_dir = "E";
    break;
    
    case SOUTH_EAST:
      s_dir = "SE";
    break;
    
    case SOUTH:
      s_dir = "S";
    break;
    
    case SOUTH_WEST:
      s_dir = "SW";
    break;
    
    case WEST:
      s_dir = "W";
    break;
    
    case NORTH_WEST:
      s_dir = "NW";
    break;

    // If the following 'case' is commented, the last valid received wind direction
    // will persist. If it's uncommented, the display will show an '*' when a 
    // received wind direction is undifined. e. g.: when the wind vane is pointing
    // between two of the eight adyacent valid cardinal directions:
    // (N, E, S, W, NE, SE, SW, NW).
//    case NOT_RECOGNIZED:
//      s_dir = "*";  // '*' will be printed to LCD to show a momentary undefined direction
//    break;
    
    default:
      // This case may never occur... check wind vane connections
      Serial.println("Error, not a valid wind direction!");
      s_dir = "x";  // 'x' will be printed to LCD to show an invalid state
    break;
  }

  return s_dir;
}


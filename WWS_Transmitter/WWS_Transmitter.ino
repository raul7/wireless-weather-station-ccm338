/* 
 * *****************************************************************************
 * Wireless Weather Station Using Low Cost ASK Modules and Arduino
 *
 *
 * Transmitting Node Hardware: 
 *                       Arduino Pro Mini ATmega328P, Generic 433/315MHz ASK RF 
 *                       Transmitter, DHT22 Relative Humidity and Temperature 
 *                       Sensor, Analog Anemometer and Analog Wind Vane.
 * Receiving Node Hardware:    
 *                       Arduino Pro Mini ATmega328P, Generic 433/315MHz ASK RF 
 *                       Receiver, 0.96" OLED SPI Display.
 * Version:              0.1 (February, 5, 2018)
 * Author:               Raul Alvarez Torrico
********************************************************************************
*/

/*  The max baud rate for these ASK modules is 2400 (perhaps up to 4800 under
 *  specific conditions), nevertheless, with higher baud rates the range for a 
 *  reliable communication without errors decreases. To achieve longer distances
 *  it is better to use baud rates equal or less than 1200.
 */

/* Include libraries */
#include <SoftwareSerial.h>       // Software serial (bit-banging) library
#include <DHT.h>                  // DHT22 sensor library

/* Define constants */
#define DEBUG_BAUD_RATE 250000    // Serial monitor debugging baud rate

// Constants for wireless communication
#define WIRELESS_BAUD_RATE 1200   // Wireless communication baud rate
#define SEND_PERIOD 2000UL        // Data send period in milliseconds

// Some constants for the communication protocol
#define SYNC_BYTE   0xAA          // For synchronizing baud rate at the receiving node
#define START_BYTE  0x7E          // Frame start delimiter
#define SOURCE_ADDRESS_BYTE 0x22  // This node's address (transmitting node)
#define DEST_ADDRESS_BYTE 0x11    // Destination node address (receiving node)

// I/O pins
#define STATUS_LED 13             // LED to show system status
#define DHT_PIN 4                 // Digital input pin for DHT22 sensor
#define ANEMOMETER_PIN A0         // Analog input pin for anemometer
#define WINDVANE_PIN A1           // Analog input pin for wind vane

// Wind vane constants representing the ADC readings to be obtained for each 
// of the voltage dividers in the wind vane circuit
#define N_VALUE   93
#define NE_VALUE  184
#define E_VALUE   236
#define SE_VALUE  327
#define S_VALUE   367
#define SW_VALUE  414
#define W_VALUE   461
#define NW_VALUE  512
// Differences between each ADC value (will pick the smallest for tolerance)
#define NE_N_DELTA  (NE_VALUE-N_VALUE)
#define E_NE_DELTA  (E_VALUE-NE_VALUE)
#define SE_E_DELTA  (SE_VALUE-E_VALUE)
#define S_SE_DELTA  (S_VALUE-SE_VALUE)
#define SW_S_DELTA  (SW_VALUE-S_VALUE)
#define W_SW_DELTA  (W_VALUE-SW_VALUE)
#define NW_W_DELTA  (NW_VALUE-W_VALUE)
#define N_NW_DELTA  (N_VALUE-NW_VALUE)
// I printed the deltas and then, picked the smallest one to use it 
// as tolerance to recognize a valid cardinal direction. After that,
// the other deltas don't have use in the rest of the code.
#define SMALLEST_DELTA  S_SE_DELTA
#define TOLERANCE (S_SE_DELTA/2)

/* Declare global variables */
float f_temperature;  // For reading temperature as a real number
float f_humidity;     // For reading humidity as a real number
int i_wind_speed;     // For reading wind velocity as an integer
int i_wind_dir;       // For reading wind direction as an integer
byte b_wind_dir;      // For encoding wind cardinal directions as numbers (1 ~ 8)
String s_wind_dir;    // For storing cardinal direction as 1-2 characters 
                      // (just for debugging)

byte id_byte = 0;     // Counter to tag frames with a unique ID number
unsigned long last_time_instant; // To store last time a transmission has been made

// Enumeration for encoding eight cardinal wind directions
enum {
  NOT_RECOGNIZED = 0,
  NORTH,
  NORTH_EAST,
  EAST,
  SOUTH_EAST,
  SOUTH,
  SOUTH_WEST,
  WEST,
  NORTH_WEST
};

/* Create objects */
DHT dht(DHT_PIN, DHT22);          // DHT22 sensor object
SoftwareSerial wlessSerial(2, 3); // SoftwareSerial object for serial wireless 
                                  // communication, pins are: (RX, TX)

/* Arduino setup function */
void setup (void)
{
  /* Initialize serial communications */
  Serial.begin(DEBUG_BAUD_RATE);          // Serial debugging
  wlessSerial.begin(WIRELESS_BAUD_RATE);  // Serial wireless comunication

  /* Initialize DHT22 sensor */
  dht.begin();
  
  /* Initialize I/O ports */
  pinMode(STATUS_LED, OUTPUT);
  
  /* Blink LED to visualize externally that setup is complete */
  Blink(STATUS_LED, 1, 400);  // (pin, num_bliks, period)
}

/* Arduino main loop */
void loop (void)
{
  /* Read temperature and humidity */
  f_temperature = dht.readTemperature();
  f_humidity    = dht.readHumidity();
  
  /* Get wind speed and wind direction */
  Get_Windspeed();
  Get_Cardinal_Wdir();

  // Print to IDE's serial monitor just for debugging
  Serial.print("f_temperature:");
  Serial.println(f_temperature);
  Serial.print("f_humidity:");
  Serial.println(f_humidity);
  
  Serial.print("Wind speed: ");
  Serial.println(i_wind_speed); // Wind speed in km/h (multiplied by 100)
  
  Serial.print("Wind dir: ");
  Serial.print(i_wind_dir);     // Wind dir as ADC value
  Serial.print('\t');
  Serial.print(b_wind_dir);     // Wind dir as encoded integer (see the defined enum)
  Serial.print('\t');
  Serial.println(s_wind_dir);   // Wind dir as a 1-2 character (just for local debugging)

  /* Send data periodically every SEND_PERIOD milliseconds */
  // This is a way to do it without blocking the rest of the code
  if (millis() - last_time_instant >= SEND_PERIOD)
  {
    last_time_instant = millis(); // Mark this new time instant
  
    /* With the following function we transmit one frame at a time. We transmit 
     *  the same frame more than once, in case the first frame gets lost or 
     *  corrupted (which may occur due to the inherent unreliability of wireless 
     *  communication); this way we improve the chance of the frame being received.
     */
    Transmit();   // Transmitting function
    
    Transmit();   // Transmitting function
  
    ++id_byte;    // After sending (twice) the current frame increment ID number 
                  // to tag the next frame with the next integer between 0 ~ 255
    
    // Blink status LED to visualize externally that a frame has been sent
    Blink(STATUS_LED, 2, 200); 
  }
}

/*******************************************************************************
*                               TRANSMIT
* Description   :    Transmits a complete frame
********************************************************************************/
void Transmit (void)
{
  // Print to IDE's serial monitor just for debugging
  Serial.println("--- Building and sending a frame ----");

  /* Send a complete frame thru software serial port, one byte at a time */
  wlessSerial.write(SYNC_BYTE); // Send the sync byte

  /* If there are synchronization issues at the receiving node, it could help
   *  to send more than one sync byte. The code at the receiving node ignores
   *  any additional sync bytes. Uncomment the following line(s) to try it:
   */
//  wlessSerial.write(SYNC_BYTE);
//  wlessSerial.write(SYNC_BYTE);

  wlessSerial.write(START_BYTE);          // Send the start byte
  wlessSerial.write(SOURCE_ADDRESS_BYTE); // Send this node's address byte
  wlessSerial.write(DEST_ADDRESS_BYTE);   // Send the receiving node's address byte
  wlessSerial.write(id_byte);             // Send the ID byte for this frame
  
  byte data_len = 7;                      // Define here the number of bytes to send
                                          // in the payload. I just counted them 
                                          // manually, it must be modified accordingly 
                                          // if the payload length is modified.
  wlessSerial.write(data_len);            // Send the data length byte

  // Multiply temperature by 100 to send the data as an integer (2 bytes) 
  // instead of a float (4 bytes), preserving 2 decimals
  int i_temperature = f_temperature*100;
  // Define a pointer to the integer temperature's memory address
  byte * p_temperature = (byte *) &i_temperature;

  // Multiply humidity by 100 to send the data as an integer (2 bytes) 
  // instead of a float (4 bytes), preserving 2 decimals
  int i_humidity = f_humidity*100;
  // Define a pointer to the integer humidity's memory address
  byte * p_humidity = (byte *) &i_humidity;

  // Define a pointer to the integer wind velocity's memory address
  // at this point the wind speed is already multiplied by 100
  // in the Get_Windspeed() funtion
  byte * p_wind_speed = (byte *) &i_wind_speed;
  
  // Send temperature, humidity and wind velocity,
  // one byte at a time, accessing memory using array indexes
  wlessSerial.write(p_temperature[0]);
  wlessSerial.write(p_temperature[1]);

  wlessSerial.write(p_humidity[0]);
  wlessSerial.write(p_humidity[1]);

  wlessSerial.write(p_wind_speed[0]);
  wlessSerial.write(p_wind_speed[1]);

  wlessSerial.write(b_wind_dir); // Send wind direction byte

  // Calculate checksum
  byte checksum = 0xFF - (p_temperature[0] + p_temperature[1] + p_humidity[0] + p_humidity[1] 
                + p_wind_speed[0] + p_wind_speed[1] + b_wind_dir);

  wlessSerial.write(checksum); // Send checksum byte

  // Print to IDE's serial monitor just for debugging
  Serial.print("checksum:");
  Serial.println(checksum);
}

/*******************************************************************************
*                                 BLINK
* Description   :    Blinks the status LED
********************************************************************************/
void Blink (byte led_pin, int times, int period) {
  for (int i = 0; i < times; i++) {
    digitalWrite(led_pin, HIGH);
    delay(period >> 2);
    digitalWrite(led_pin, LOW);
    delay(period >> 2);
  }
}

/*******************************************************************************
*                              GET_CARDINAL_WDIR
* Description   :    Calculates the cardinal wind direction
*                    from wind vane reading
********************************************************************************/
void Get_Cardinal_Wdir (void)
{
  // Read analog value from wind vane
  i_wind_dir = analogRead(WINDVANE_PIN);

  /* We must check in which tolerance range read value is located to determine
   *  the wind direction. Remember these definitions at the beggining of the code:
      #define N_VALUE   93
      #define NE_VALUE  184
      #define E_VALUE   236
      #define SE_VALUE  327
      #define S_VALUE   367
      #define SW_VALUE  414
      #define W_VALUE   461
      #define NW_VALUE  512
   *  
   *  Range of tolerances for a each cardinal direction are defined taking one of the
   *  above values and adding, substracting the half of smallest delta between them 
   *  (defined by TOLERANCE). e. g.:  For North the range will be defined by: 
   *  (N_VALUE - TOLERANCE, N_VALUE + TOLERANCE, and so forth for the rest.
   */

  // Check if the read value is in the range defined for 'North'
  if ((i_wind_dir > (N_VALUE - TOLERANCE))
    && (i_wind_dir < (N_VALUE + TOLERANCE)))
    {
      b_wind_dir = NORTH;
      s_wind_dir = "North";
    }

    // Check if the read value is in the range defined for 'North East'
    else if ((i_wind_dir > (NE_VALUE - TOLERANCE))
    && (i_wind_dir < (NE_VALUE + TOLERANCE)))
    {
      b_wind_dir = NORTH_EAST;
      s_wind_dir = "North East";
    }

    // Check if the read value is in the range defined for 'East'
    else if ((i_wind_dir > (E_VALUE - TOLERANCE))
    && (i_wind_dir < (E_VALUE + TOLERANCE)))
    {
      b_wind_dir = EAST;
      s_wind_dir = "East";
    }

    // Check if the read value is in the range defined for 'South East'
    else if ((i_wind_dir > (SE_VALUE - TOLERANCE))
    && (i_wind_dir < (SE_VALUE + TOLERANCE)))
    {
      b_wind_dir = SOUTH_EAST;
      s_wind_dir = "South East";
    }

    // Check if the read value is in the range defined for 'South'
    else if ((i_wind_dir > (S_VALUE - TOLERANCE))
    && (i_wind_dir < (S_VALUE + TOLERANCE)))
    {
      b_wind_dir = SOUTH;
      s_wind_dir = "South";
    }

    // Check if the read value is in the range defined for 'South West'
    else if ((i_wind_dir > (SW_VALUE - TOLERANCE))
    && (i_wind_dir < (SW_VALUE + TOLERANCE)))
    {
      b_wind_dir = SOUTH_WEST;
      s_wind_dir = "South West";
    }

    // Check if the read value is in the range defined for 'West'
    else if ((i_wind_dir > (W_VALUE - TOLERANCE))
    && (i_wind_dir < (W_VALUE + TOLERANCE)))
    {
      b_wind_dir = WEST;
      s_wind_dir = "West";
    }

    // Check if the read value is in the range defined for 'North West'
    else if ((i_wind_dir > (NW_VALUE - TOLERANCE))
    && (i_wind_dir < (NW_VALUE + TOLERANCE)))
    {
      b_wind_dir = NORTH_WEST;
      s_wind_dir = "North West";
    }
    
    else
    {
      // This is the case when the read wind direction is undefined. 
      // e. g.: when the wind vane is pointing between two of the eight adyacent
      // valid cardinal directions: (N, E, S, W, NE, SE, SW, NW).
      b_wind_dir = NOT_RECOGNIZED;
      s_wind_dir = "Not recognized";
    }
}

/*******************************************************************************
*                              GET_WINDSPEED
* Description   :    Calculates the wind speed
*                    from the anemometer analog reading
********************************************************************************/
void Get_Windspeed (void)
{
  /* To calibrate the anemometer, I drove mi car with it out of the window
   *  (in a calm day) at the following speeds and obtained the following ADC
   *  readings:
   *          10kmh -> 25 units
   *          20kmh -> 40 units
   *          30kmh -> 70 units
   * 
   * Then, I calculated a function using the Linear Regression tool from this
   * site: https://www.graphpad.com/quickcalcs/linear1/
   * And I obtained the following linear function to calculate all wind speeds,
   * for ADC readings in the range of 0~1023 (10-bit ADC converter):
   *          y = 2.25*x     ;     x: km/h, y: ADC reading
   *          
   * To calculate km/h we will use, then:
   *          x = y / 2.25;
   */

  unsigned int adc_reading = analogRead(ANEMOMETER_PIN);

  // Use the mapping equation and then multiply the result by 100, 
  // to send the data as an unsigned integer (2 bytes) instead of 
  // as a float (4 bytes), preserving 2 decimals
  i_wind_speed = (adc_reading/2.25)*100;
}

